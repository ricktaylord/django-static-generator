from distutils.core import setup

version = '1.1'

setup(name='django-staticgenerator',
      version=version,
      description="StaticGenerator for Django - fork",
      author="Rick Taylor",
      author_email="rick@ricktaylordesign.co.uk",
      url="https://github.com/mredar/django-staticgenerator",
      packages = ['staticgenerator']
      )
